package com.webinfotech.spicepay.respository;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AppRepository {

    @POST("login/check")
    @FormUrlEncoded
    Call<ResponseBody> checkUser(@Field("email") String email);

    @POST("user/address/update")
    @FormUrlEncoded
    Call<ResponseBody> updateAddress(@Header("Authorization") String authorization,
                                     @Field("user_id") int userId,
                                     @Field("name") String name,
                                     @Field("dob") String dob,
                                     @Field("address") String address
                                     );

    @POST("user/account/update")
    @FormUrlEncoded
    Call<ResponseBody> updateBankAccount(@Header("Authorization") String authorization,
                                         @Field("user_id") int userId,
                                         @Field("account_no") String accountNo,
                                         @Field("ifsc") String ifsc,
                                         @Field("bank_name") String bankName,
                                         @Field("bank_holder_name") String bankHolderName,
                                         @Field("account_type") int accountType
    );

    @POST("user/kyc/update")
    @Multipart
    Call<ResponseBody> updateKyc(  @Header("Authorization") String authorization,
                                   @Part("user_id") RequestBody userId,
                                   @Part MultipartBody.Part profilePhoto,
                                   @Part MultipartBody.Part pan,
                                   @Part MultipartBody.Part aadharFront,
                                   @Part MultipartBody.Part aadharBack
    );

}
