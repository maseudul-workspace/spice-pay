package com.webinfotech.spicepay.respository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.spicepay.domain.models.CommonResponse;
import com.webinfotech.spicepay.domain.models.UserInfoWrapper;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Header;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public UserInfoWrapper checkLogin(String email){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkUser(email);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return userInfoWrapper;
    }

    public CommonResponse updateAddress(String authorization,
                                        int userId,
                                        String name,
                                        String dob,
                                        String address) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateAddress("Bearer " + authorization, userId, name, dob, address);

            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse updateBankAccount(String authorization,
                                        int userId,
                                        String accountNo,
                                        String ifsc,
                                        String bankName,
                                        String bankHolderName,
                                        int accountType
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateBankAccount("Bearer " + authorization, userId, accountNo, ifsc, bankName, bankHolderName, accountType);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

    public CommonResponse updateKyc(String apiToken, int userId, String profilePhoto, String pan, String aadharFront, String aadharBack) {
        RequestBody userIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(userId));
        MultipartBody.Part profilePhotoFileBody = null;
        if (profilePhoto != null) {
            File imageFile = new File(profilePhoto);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            profilePhotoFileBody = MultipartBody.Part.createFormData("photo", imageFile.getName(), requestImageFile);
        }

        MultipartBody.Part panFileBody = null;
        if (pan != null) {
            File imageFile = new File(pan);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            panFileBody = MultipartBody.Part.createFormData("pan", imageFile.getName(), requestImageFile);
        }

        MultipartBody.Part aadharFrontFileBody = null;
        if (aadharFront != null) {
            File imageFile = new File(aadharFront);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            aadharFrontFileBody = MultipartBody.Part.createFormData("aadhar_front", imageFile.getName(), requestImageFile);
        }

        MultipartBody.Part aadharBackFileBody = null;
        if (aadharBack != null) {
            File imageFile = new File(aadharBack);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            aadharBackFileBody = MultipartBody.Part.createFormData("aadhar_back", imageFile.getName(), requestImageFile);
        }

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateKyc("Bearer " + apiToken, userIdRequestBody, profilePhotoFileBody, panFileBody, aadharFrontFileBody, aadharBackFileBody);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
                Log.e("LogMsg", "Response Body: " + responseBody);
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
            Log.e("LogMsg", "Exception: " + e.getMessage());
        }
        return commonResponse;
    }

}
