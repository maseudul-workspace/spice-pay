package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.executors.impl.ThreadExecutor;
import com.webinfotech.spicepay.presentation.presenters.ProfileCompletePresenter;
import com.webinfotech.spicepay.presentation.presenters.impl.ProfileCompletePresenterImpl;
import com.webinfotech.spicepay.threading.MainThreadImpl;

import java.util.Calendar;

public class CompleteProfileActivity extends AppCompatActivity implements ProfileCompletePresenter.View {

    @BindView(R.id.view_circle_2_active)
    View viewCircle2Active;
    @BindView(R.id.view_circle_2_inactive)
    View viewCircle2InActive;
    @BindView(R.id.view_line_1_active)
    View viewLine1Active;
    @BindView(R.id.view_line_1_inactive)
    View viewLine1InActive;
    @BindView(R.id.view_line_2_active)
    View viewLine2Active;
    @BindView(R.id.view_line_2_inactive)
    View viewLine2InActive;
    @BindView(R.id.layout_user_form)
    View layoutUserForm;
    @BindView(R.id.layout_bank_details_form)
    View layoutBankDetailsForm;
    @BindView(R.id.layout_kyc_update_form)
    View layoutKycUpdateForm;
    @BindView(R.id.btn_next_1)
    Button btnNext1;
    @BindView(R.id.layout_btn_container)
    View layoutBtnContainer;
    @BindView(R.id.layout_btn_container2)
    View layoutBtnContainer2;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_dob)
    EditText editTextDob;
    @BindView(R.id.edit_text_bank_name)
    EditText editTextBankName;
    @BindView(R.id.edit_text_account_no)
    EditText editTextAccountNo;
    @BindView(R.id.edit_text_ifsc_code)
    EditText editTextIfscCode;
    @BindView(R.id.edit_text_bank_holder_name)
    EditText editTextBankHolderName;
    @BindView(R.id.radio_group_account_type)
    RadioGroup radioGroupAccountType;
    int accountType = 0;
    ProgressDialog progressDialog;
    private String dob;
    private ProfileCompletePresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_profile);
        ButterKnife.bind(this);
        setUpProgressDialog();
        setRadioGroupAccountType();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new ProfileCompletePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setRadioGroupAccountType() {
        radioGroupAccountType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_saving:
                        accountType = 1;
                        break;
                    case R.id.radio_btn_current:
                        accountType = 2;
                        break;
                }
            }
        });
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait");
    }

    private void showUserForm() {
        viewLine1Active.setVisibility(View.GONE);
        viewCircle2Active.setVisibility(View.GONE);
        viewCircle2InActive.setVisibility(View.VISIBLE);
        viewLine1InActive.setVisibility(View.VISIBLE);
        viewLine2Active.setVisibility(View.GONE);
        viewLine2InActive.setVisibility(View.VISIBLE);

        layoutBtnContainer2.setVisibility(View.GONE);
        btnNext1.setVisibility(View.VISIBLE);
        layoutBtnContainer.setVisibility(View.GONE);

        layoutUserForm.setVisibility(View.VISIBLE);
        layoutBankDetailsForm.setVisibility(View.GONE);
        layoutKycUpdateForm.setVisibility(View.GONE);

    }

    private void showBankForm() {
        viewLine1Active.setVisibility(View.VISIBLE);
        viewCircle2Active.setVisibility(View.GONE);
        viewCircle2InActive.setVisibility(View.VISIBLE);
        viewLine1InActive.setVisibility(View.GONE);
        viewLine2Active.setVisibility(View.GONE);
        viewLine2InActive.setVisibility(View.VISIBLE);

        layoutBtnContainer2.setVisibility(View.GONE);
        btnNext1.setVisibility(View.GONE);
        layoutBtnContainer.setVisibility(View.VISIBLE);

        layoutUserForm.setVisibility(View.GONE);
        layoutBankDetailsForm.setVisibility(View.VISIBLE);
        layoutKycUpdateForm.setVisibility(View.GONE);

    }

    private void showKycForm() {
        viewLine1Active.setVisibility(View.VISIBLE);
        viewCircle2Active.setVisibility(View.VISIBLE);
        viewCircle2InActive.setVisibility(View.GONE);
        viewLine1InActive.setVisibility(View.GONE);
        viewLine2Active.setVisibility(View.VISIBLE);
        viewLine2InActive.setVisibility(View.GONE);

        layoutBtnContainer2.setVisibility(View.VISIBLE);
        btnNext1.setVisibility(View.GONE);
        layoutBtnContainer.setVisibility(View.GONE);

        layoutUserForm.setVisibility(View.GONE);
        layoutBankDetailsForm.setVisibility(View.GONE);
        layoutKycUpdateForm.setVisibility(View.VISIBLE);

    }

    @OnClick(R.id.btn_next_1) void onBtnNext1Clicked() {
        if (editTextName.getText().toString().trim().isEmpty() || editTextAddress.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Fill All The Fields").show();
        } else if (editTextDob.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please Set Your DOB").show();
        } else {
            mPresenter.updateAddress(editTextName.getText().toString(), dob, editTextAddress.getText().toString());
        }
    }

    @OnClick(R.id.btn_next_2) void onBtnNext2Clicked() {
        if (    editTextBankHolderName.getText().toString().trim().isEmpty() ||
                editTextBankName.getText().toString().trim().isEmpty() ||
                editTextAccountNo.getText().toString().trim().isEmpty() ||
                editTextIfscCode.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Please Fill All The Fields").show();
        } else if (accountType == 0) {
            Toasty.warning(this, "Please Choose Your Account Type").show();
        } else {
            mPresenter.updateBankAccount(editTextAccountNo.getText().toString(), editTextIfscCode.getText().toString(), editTextBankName.getText().toString(), editTextBankHolderName.getText().toString(), accountType);
        }
    }

    @OnClick(R.id.btn_prev) void onPrevClicked() {
        showUserForm();
    }

    @OnClick(R.id.btn_prev_2) void onPrev2Clicked() {
        showBankForm();
    }

    @OnClick(R.id.btn_finish) void onFinishClicked() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onAddressUpdateSuccess() {
        showBankForm();
    }

    @Override
    public void onBankAccountUpdateSuccess() {
        showKycForm();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.edit_text_dob) void onDobClicked() {
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        editTextDob.setText(dob);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

}