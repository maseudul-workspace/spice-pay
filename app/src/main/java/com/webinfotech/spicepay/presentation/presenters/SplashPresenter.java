package com.webinfotech.spicepay.presentation.presenters;

public interface SplashPresenter {
    void checkLogin(String email);
    interface View {
        void goToMainActivity();
        void goToCompleteProfileActivity();
    }
}
