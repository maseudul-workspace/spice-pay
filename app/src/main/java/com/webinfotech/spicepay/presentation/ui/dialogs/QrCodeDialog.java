package com.webinfotech.spicepay.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.util.GlideHelper;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QrCodeDialog {

    public interface Callback {
        void onDownloadClicked();
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.img_view_qr_code)
    ImageView imgViewQrCode;

    public QrCodeDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_qr_code_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setImgViewQrCode(String url) {
        GlideHelper.setImageView(mContext, imgViewQrCode, url);
    }

    @OnClick(R.id.btn_download) void onDownloadClicked() {
        hideDialog();
        mCallback.onDownloadClicked();
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
