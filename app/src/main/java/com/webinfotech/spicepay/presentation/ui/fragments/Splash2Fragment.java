package com.webinfotech.spicepay.presentation.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.spicepay.R;


public class Splash2Fragment extends Fragment {



    public Splash2Fragment() {
        // Required empty public constructor
    }

    public static Splash2Fragment newInstance(String param1, String param2) {
        Splash2Fragment fragment = new Splash2Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash2, container, false);
    }
}