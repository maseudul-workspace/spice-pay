package com.webinfotech.spicepay.presentation.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.spicepay.R;


public class Splash1Fragment extends Fragment {

    public Splash1Fragment() {
        // Required empty public constructor
    }


    public static Splash1Fragment newInstance(String param1, String param2) {
        Splash1Fragment fragment = new Splash1Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash1, container, false);
    }

}