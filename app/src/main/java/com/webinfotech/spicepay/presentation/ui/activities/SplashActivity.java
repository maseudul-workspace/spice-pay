package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.executors.impl.ThreadExecutor;
import com.webinfotech.spicepay.presentation.presenters.SplashPresenter;
import com.webinfotech.spicepay.presentation.presenters.impl.SplashPresenterImpl;
import com.webinfotech.spicepay.threading.MainThreadImpl;

public class SplashActivity extends AppCompatActivity implements SplashPresenter.View {

    SplashPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initialisePresenter();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkGoogleSignIn();
            }
        }, 5000);
    }

    private void initialisePresenter() {
        mPresenter = new SplashPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void checkGoogleSignIn() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            mPresenter.checkLogin(account.getEmail());
        } else {
            goToSplashSliderActivity();
        }
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToCompleteProfileActivity() {
        Intent intent = new Intent(this, CompleteProfileActivity.class);
        startActivity(intent);
    }

    private void goToSplashSliderActivity() {
        Intent intent = new Intent(this, SplashSliderActivity.class);
        startActivity(intent);
        finish();
    }

}