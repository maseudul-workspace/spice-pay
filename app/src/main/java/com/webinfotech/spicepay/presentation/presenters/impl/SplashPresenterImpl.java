package com.webinfotech.spicepay.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.spicepay.AndroidApplication;
import com.webinfotech.spicepay.domain.executors.Executor;
import com.webinfotech.spicepay.domain.executors.MainThread;
import com.webinfotech.spicepay.domain.interactors.CheckLoginInteractor;
import com.webinfotech.spicepay.domain.interactors.impl.CheckLoginInteractorImpl;
import com.webinfotech.spicepay.domain.models.UserInfo;
import com.webinfotech.spicepay.presentation.presenters.SplashPresenter;
import com.webinfotech.spicepay.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.spicepay.respository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SplashPresenterImpl extends AbstractPresenter implements SplashPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    SplashPresenter.View mView;

    public SplashPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String email) {
        CheckLoginInteractorImpl checkLoginInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, email);
        checkLoginInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo, boolean isProfileComplete) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        if (isProfileComplete) {
            mView.goToMainActivity();
        } else {
            mView.goToCompleteProfileActivity();
        }
    }

    @Override
    public void onLoginFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg).show();
    }
}
