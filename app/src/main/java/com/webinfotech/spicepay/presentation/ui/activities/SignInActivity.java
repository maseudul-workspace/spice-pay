package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.tasks.Task;
import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.executors.impl.ThreadExecutor;
import com.webinfotech.spicepay.presentation.presenters.SignInPresenter;
import com.webinfotech.spicepay.presentation.presenters.impl.SignInPresenterImpl;
import com.webinfotech.spicepay.threading.MainThreadImpl;

public class SignInActivity extends AppCompatActivity implements SignInPresenter.View {

    @BindView(R.id.sign_in_button)
    SignInButton signInButton;
    GoogleSignInOptions gso;
    private GoogleSignInClient mGoogleSignInClient;
    private int RC_SIGN_IN = 1200;
    private SignInPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        setUpGoogleSignIn();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new SignInPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpGoogleSignIn() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @OnClick(R.id.sign_in_button) void onSignInBtnClicked() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        mPresenter.checkLogin(task.getResult().getEmail());
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void goToCompleteProfileActivity() {
        Intent intent = new Intent(this, CompleteProfileActivity.class);
        startActivity(intent);
    }
}