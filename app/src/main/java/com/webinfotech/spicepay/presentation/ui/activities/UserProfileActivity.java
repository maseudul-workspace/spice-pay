package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.service.autofill.TextValueSanitizer;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.avatarfirst.avatargenlib.AvatarConstants;
import com.avatarfirst.avatargenlib.AvatarGenerator;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.util.GlideHelper;

public class UserProfileActivity extends AppCompatActivity {

    @BindView(R.id.img_view_user)
    ImageView imgViewUser;
    @BindView(R.id.txt_view_user_email)
    TextView txtViewUserEmail;
    @BindView(R.id.txt_view_user_name)
    TextView txtViewUserName;
    @BindView(R.id.txt_view_user_phone)
    TextView txtViewUserPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        setData();
    }

    private void setData() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            txtViewUserEmail.setText(account.getEmail());
            txtViewUserName.setText(account.getDisplayName());
            if (account.getPhotoUrl() != null) {
                GlideHelper.setImageViewWithURI(this, imgViewUser, account.getPhotoUrl());
            } else {
                imgViewUser.setImageDrawable(AvatarGenerator.Companion.avatarImage(
                        this,
                        200,
                        AvatarConstants.Companion.getCIRCLE(),
                        account.getDisplayName()
                ));
            }
        }
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}