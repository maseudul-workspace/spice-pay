package com.webinfotech.spicepay.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.spicepay.AndroidApplication;
import com.webinfotech.spicepay.domain.executors.Executor;
import com.webinfotech.spicepay.domain.executors.MainThread;
import com.webinfotech.spicepay.domain.interactors.UpdateAddressInteractor;
import com.webinfotech.spicepay.domain.interactors.UpdateBankInteractor;
import com.webinfotech.spicepay.domain.interactors.impl.UpdateAddressInteractorImpl;
import com.webinfotech.spicepay.domain.interactors.impl.UpdateBankInteractorImpl;
import com.webinfotech.spicepay.domain.models.UserInfo;
import com.webinfotech.spicepay.presentation.presenters.ProfileCompletePresenter;
import com.webinfotech.spicepay.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.spicepay.respository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ProfileCompletePresenterImpl extends AbstractPresenter implements ProfileCompletePresenter,
                                                                                UpdateAddressInteractor.Callback,
                                                                                UpdateBankInteractor.Callback
{

    Context mContext;
    ProfileCompletePresenter.View mView;

    public ProfileCompletePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void updateAddress(String name, String dob, String address) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            UpdateAddressInteractorImpl updateAddressInteractor = new UpdateAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, name, dob, address);
            updateAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateBankAccount(String accountNo, String ifsc, String bankName, String bankHolderName, int accountType) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            UpdateBankInteractorImpl updateBankInteractor = new UpdateBankInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, accountNo, ifsc, bankName, bankHolderName, accountType);
            updateBankInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddressUpdateSuccess() {
        mView.hideLoader();
        mView.onAddressUpdateSuccess();
    }

    @Override
    public void onAddressUpdateFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_LONG).show();
    }

    @Override
    public void onBankUpdateSuccess() {
        mView.hideLoader();
        mView.onBankAccountUpdateSuccess();
    }

    @Override
    public void onBankUpdateFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_LONG).show();
    }
}
