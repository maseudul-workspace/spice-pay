package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.models.testing.Image;
import com.webinfotech.spicepay.presentation.ui.adapters.MainViewPagerAdapter;
import com.webinfotech.spicepay.presentation.ui.dialogs.QrCodeDialog;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends BaseActivity implements QrCodeDialog.Callback {

    @BindView(R.id.main_view_pager)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;
    QrCodeDialog qrCodeDialog;
    public static final int PERMISSION_WRITE = 0;
    ProgressDialog progressDialog;
    private GoogleSignInOptions gso;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        setViewPager();
        setQrCodeDialog();
        setUpGoogleSignIn();
    }

    private void setUpGoogleSignIn() {
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setQrCodeDialog() {
        qrCodeDialog = new QrCodeDialog(this, this, this::onDownloadClicked);
        qrCodeDialog.setUpDialog();
    }

    private void setViewPager() {
        ArrayList<Image> images = new ArrayList<>();
        Image image1 = new Image("https://apps.epom.com/blog/img/smart-banner-vs-standard-banner-for-effective-app-monetization/bg-full.png");
        Image image2 = new Image("https://www.computerra.ru/wp-content/uploads/2018/09/1080-h-580-1.png");
        Image image3 = new Image("https://i.ytimg.com/vi/2Iv7cJxatXY/maxresdefault.jpg");
        Image image4 = new Image("https://blog.intelligentbee.com/wp-content/uploads/2019/03/Generate-Revenue-from-Mobile-App-Banner1.png");

        images.add(image1);
        images.add(image2);
        images.add(image3);
        images.add(image4);

        MainViewPagerAdapter adapter = new MainViewPagerAdapter(this, images);
        viewPager.setAdapter(adapter);

        dotsIndicator.setViewPager(viewPager);

    }

    @OnClick(R.id.layout_send_money) void onSendMoneyClicked() {
        goToQrCodeActivity();
    }

    @OnClick(R.id.layout_update_transaction) void onUpdateTransactionClicked() {
        Intent intent = new Intent(this, AddTransactionActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_user_profile) void onUserProfileClicked() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_update_kyc) void onUpdateKycClicked() {
        Intent intent = new Intent(this, KycUpdateActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_transaction_list) void onTransactionListClicked() {
        Intent intent = new Intent(this, TransactionsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_log_out) void onLayoutLogOutClicked() {
        signOut();
    }


    private void showQrCodeDialog() {
        qrCodeDialog.setImgViewQrCode("https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/QR-code-obituary.svg/768px-QR-code-obituary.svg.png");
        qrCodeDialog.showDialog();
    }

    private void goToQrCodeActivity() {
        Intent intent = new Intent(this, QrCodeActivity.class);
        startActivity(intent);
    }

    private void signOut() {
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent intent = new Intent(getApplicationContext(), SignInActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    @Override
    public void onDownloadClicked() {
        if (checkPermission()) {
            new Downloading().execute("https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/QR-code-obituary.svg/768px-QR-code-obituary.svg.png");
        }
    }

    public class Downloading extends AsyncTask<String, Integer, String> {

        @Override
        public void onPreExecute() {
            super .onPreExecute();
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... url) {
            File mydir = new File(Environment.getExternalStorageDirectory() + "/SpicePay");
            if (!mydir.exists()) {
                mydir.mkdirs();
            }

            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(url[0]);
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);

            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setTitle("Downloading")
                    .setDestinationInExternalPublicDir("/SpicePay", "qrCode.jpg");

            manager.enqueue(request);
            return mydir.getAbsolutePath() + File.separator + "qrCode.jpg";
        }

        @Override
        public void onPostExecute(String s) {
            super .onPostExecute(s);
            progressDialog.dismiss();
            Toasty.success(getApplicationContext(), "Image Saved", Toast.LENGTH_LONG).show();
        }
    }

    //runtime storage permission
    public boolean checkPermission() {
        int READ_EXTERNAL_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if((READ_EXTERNAL_PERMISSION != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_WRITE);
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==PERMISSION_WRITE && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            //do somethings
        }
    }

}