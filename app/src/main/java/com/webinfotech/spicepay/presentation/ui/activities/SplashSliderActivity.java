package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.presentation.ui.adapters.FragmentsAdapter;
import com.webinfotech.spicepay.presentation.ui.fragments.Splash1Fragment;
import com.webinfotech.spicepay.presentation.ui.fragments.Splash2Fragment;
import com.webinfotech.spicepay.presentation.ui.fragments.Splash3Fragment;
import com.webinfotech.spicepay.util.GlideHelper;

public class SplashSliderActivity extends AppCompatActivity {

    @BindView(R.id.view_pager_splash)
    ViewPager viewPagerSplash;
    @BindView(R.id.dots_indicator)
    LinearLayout dotsLayout;
    @BindView(R.id.btn_get_started)
    Button btnGetStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_slider);
        ButterKnife.bind(this);
        setViewPagerSplash();
    }

    private void setViewPagerSplash() {

        Splash1Fragment splash1Fragment = new Splash1Fragment();
        Splash2Fragment splash2Fragment = new Splash2Fragment();
        Splash3Fragment splash3Fragment = new Splash3Fragment();

        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());
        adapter.AddFragment(splash1Fragment, "");
        adapter.AddFragment(splash2Fragment, "");
        adapter.AddFragment(splash3Fragment, "");

        viewPagerSplash.setAdapter(adapter);

        prepareDotsIndicator(0);

        viewPagerSplash.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 2) {
                    btnGetStarted.setVisibility(View.VISIBLE);
                } else {
                    btnGetStarted.setVisibility(View.INVISIBLE);
                }
                prepareDotsIndicator(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void prepareDotsIndicator(int sliderPosition){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[3];
        for(int i = 0; i < 3; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(12, 12);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

    @OnClick(R.id.btn_get_started) void onGetStartedClicked() {
        Intent intent = new Intent(this, CompleteProfileActivity.class);
        startActivity(intent);
    }
    
}