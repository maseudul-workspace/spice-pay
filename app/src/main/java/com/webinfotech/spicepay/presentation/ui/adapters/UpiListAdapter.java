package com.webinfotech.spicepay.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.models.testing.UpiList;
import com.webinfotech.spicepay.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class UpiListAdapter extends RecyclerView.Adapter<UpiListAdapter.ViewHolder> {

    Context mContext;
    ArrayList<UpiList> upiLists;

    public UpiListAdapter(Context mContext, ArrayList<UpiList> upiLists) {
        this.mContext = mContext;
        this.upiLists = upiLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_upi_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUpiName.setText(upiLists.get(position).title);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewUpi, upiLists.get(position).upiImage, 100);
    }

    @Override
    public int getItemCount() {
        return upiLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_upi_name)
        TextView txtViewUpiName;
        @BindView(R.id.img_view_upi)
        ImageView imgViewUpi;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
