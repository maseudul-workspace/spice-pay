package com.webinfotech.spicepay.presentation.presenters;

public interface ProfileCompletePresenter {
    void updateAddress(String name, String dob, String address);
    void updateBankAccount( String accountNo,
                            String ifsc,
                            String bankName,
                            String bankHolderName,
                            int accountType);
    interface View {
        void onAddressUpdateSuccess();
        void onBankAccountUpdateSuccess();
        void showLoader();
        void hideLoader();
    }
}
