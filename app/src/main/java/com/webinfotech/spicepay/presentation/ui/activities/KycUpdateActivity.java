package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ImageView;
import android.widget.Toast;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.webinfotech.spicepay.util.Helper.calculateFileSize;
import static com.webinfotech.spicepay.util.Helper.getRealPathFromURI;
import static com.webinfotech.spicepay.util.Helper.saveImage;

public class KycUpdateActivity extends AppCompatActivity {

    @BindView(R.id.img_view_profile_pic)
    ImageView imgViewProfilePic;
    @BindView(R.id.img_view_aadhar_card)
    ImageView imgViewAadharCard;
    @BindView(R.id.img_view_pan_card)
    ImageView imgViewPanCard;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    int REQUEST_PIC;
    String profilePath;
    String panPath;
    String aadharpath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kyc_update);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.layout_profile_picture) void onProfilePicLayoutClicked() {
        REQUEST_PIC = 1000;
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    @OnClick(R.id.layout_pan_card) void onPanCardLayoutClicked() {
        REQUEST_PIC = 1001;
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    @OnClick(R.id.layout_aadhar_card) void onAadharCardLayoutClicked() {
        REQUEST_PIC = 1002;
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    private void loadImageChooser() {

        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                loadImageChooser();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1000:
                    if (data.getData() != null) {
                        profilePath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(profilePath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewProfilePic, data.getData(), 150);
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        profilePath = saveImage(photo);
                        if (calculateFileSize(profilePath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewProfilePic, photo, 150);
                        }
                    }
                    break;
                case 1001:
                    if (data.getData() != null) {
                        panPath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(panPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewPanCard, data.getData(), 150);
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        panPath = saveImage(photo);
                        if (calculateFileSize(panPath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewPanCard, photo, 150);
                        }
                    }
                    break;
                case 1002:
                    if (data.getData() != null) {
                        aadharpath = getRealPathFromURI(data.getData(), this);
                        if (calculateFileSize(aadharpath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithUri(this, imgViewAadharCard, data.getData(), 150);
                        }
                    } else {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        aadharpath = saveImage(photo);
                        if (calculateFileSize(aadharpath) > 2) {
                            Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                        } else {
                            GlideHelper.setImageViewCustomRoundedCornersWithBitmap(this, imgViewAadharCard, photo, 150);
                        }
                    }
                    break;
            }
        }
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}