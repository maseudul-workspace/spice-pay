package com.webinfotech.spicepay.presentation.presenters;

public interface SignInPresenter {
    void checkLogin(String email);
    interface View {
        void goToMainActivity();
        void goToCompleteProfileActivity();
    }
}
