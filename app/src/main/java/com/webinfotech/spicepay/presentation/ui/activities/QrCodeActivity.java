package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.Manifest;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.widget.ImageView;
import android.widget.Toast;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.models.testing.UpiList;
import com.webinfotech.spicepay.presentation.ui.adapters.UpiListAdapter;
import com.webinfotech.spicepay.util.GlideHelper;

import java.io.File;
import java.util.ArrayList;

public class QrCodeActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view_upis)
    RecyclerView recyclerViewUpis;
    @BindView(R.id.img_view_qr_code)
    ImageView imgViewQrCode;
    ProgressDialog progressDialog;
    public static final int PERMISSION_WRITE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        ButterKnife.bind(this);
        progressDialog = new ProgressDialog(this);
        GlideHelper.setImageView(this, imgViewQrCode, "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/QR_code_for_mobile_English_Wikipedia.svg/1200px-QR_code_for_mobile_English_Wikipedia.svg.png");
        setRecyclerViewUpis();
    }

    @OnClick(R.id.btn_download) void onDownloadClicked() {
        if (checkPermission()) {
            new Downloading().execute("https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/QR-code-obituary.svg/768px-QR-code-obituary.svg.png");
        }
    }

    //runtime storage permission
    public boolean checkPermission() {
        int READ_EXTERNAL_PERMISSION = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        if((READ_EXTERNAL_PERMISSION != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_WRITE);
            return false;
        }
        return true;
    }

    private void setRecyclerViewUpis() {

        ArrayList<UpiList> upiLists = new ArrayList<>();

        UpiList upiList1 = new UpiList("https://cdn.iconscout.com/icon/free/png-256/google-pay-2038779-1721670.png", "Google Pay");
        UpiList upiList2 = new UpiList("https://www.searchpng.com/wp-content/uploads/2018/11/phone-pe.png", "Phone Pe");
        UpiList upiList3 = new UpiList("https://ih1.redbubble.net/image.866385666.7298/st,small,507x507-pad,600x600,f8f8f8.u5.jpg", "PayTm");
        UpiList upiList4 = new UpiList("https://cdn3.iconfinder.com/data/icons/cute-flat-social-media-icons-3/512/whatsapp.png", "Whatsapp");
        UpiList upiList5 = new UpiList("https://1.bp.blogspot.com/-_BUqH0JMrDY/XjhEVaNoz9I/AAAAAAAAYUk/iJXJaKo8Dbo9fVy1HgUeCBEWW7IHjxLSgCLcBGAsYHQ/s1600/Download%2BScanPay%2B-%2BOne%2BApp%2BFor%2BAll%2BPayment%2Bmobile%2Bapp.JPG", "Scanpay");
        UpiList upiList6 = new UpiList("https://upload.wikimedia.org/wikipedia/commons/d/de/Amazon_icon.png", "Amazon");

        upiLists.add(upiList1);
        upiLists.add(upiList2);
        upiLists.add(upiList3);
        upiLists.add(upiList4);
        upiLists.add(upiList5);
        upiLists.add(upiList6);

        UpiListAdapter adapter = new UpiListAdapter(this, upiLists);
        recyclerViewUpis.setLayoutManager(new GridLayoutManager(this, 3));
        recyclerViewUpis.setAdapter(adapter);

    }

    public class Downloading extends AsyncTask<String, Integer, String> {

        @Override
        public void onPreExecute() {
            super .onPreExecute();
            progressDialog.setMessage("Please wait");
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... url) {
            File mydir = new File(Environment.getExternalStorageDirectory() + "/SpicePay");
            if (!mydir.exists()) {
                mydir.mkdirs();
            }

            DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(url[0]);
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);

            request.setAllowedNetworkTypes(
                    DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setTitle("Downloading")
                    .setDestinationInExternalPublicDir("/SpicePay", "qrCode.jpg");

            manager.enqueue(request);
            return mydir.getAbsolutePath() + File.separator + "qrCode.jpg";
        }

        @Override
        public void onPostExecute(String s) {
            super .onPostExecute(s);
            progressDialog.dismiss();
            Toasty.success(getApplicationContext(), "Image Saved", Toast.LENGTH_LONG).show();
        }
    }

}