package com.webinfotech.spicepay.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.models.testing.Transaction;
import com.webinfotech.spicepay.presentation.ui.adapters.TransactionAdapter;

import java.util.ArrayList;

public class TransactionsActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view_transactions)
    RecyclerView recyclerViewTransactions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        ButterKnife.bind(this);
        setRecyclerViewTransactions();
    }

    private void setRecyclerViewTransactions() {
        ArrayList<Transaction> transactions = new ArrayList<>();
        Transaction transaction1 = new Transaction("https://randomuser.me/api/portraits/men/40.jpg", "Peter Parker", "14 February", "Valentine Gift", "1200");
        Transaction transaction2 = new Transaction("https://randomuser.me/api/portraits/men/9.jpg", "Bruce Wayne", "18 February", "House Rent", "2500");
        Transaction transaction3 = new Transaction("https://randomuser.me/api/portraits/women/77.jpg", "Miley Cyrys", "10 December", "", "500");
        Transaction transaction4 = new Transaction("https://randomuser.me/api/portraits/men/17.jpg", "John Rambo", "25 November", "Previous Loan", "1002");
        Transaction transaction5 = new Transaction("https://randomuser.me/api/portraits/men/18.jpg", "Alex Mercer", "5 March", "", "2500");
        Transaction transaction6 = new Transaction("https://randomuser.me/api/portraits/women/66.jpg", "Maria Kenelis", "30 March", "Birthday Gift", "3000");
        Transaction transaction7 = new Transaction("https://randomuser.me/api/portraits/women/47.jpg", "Lisa Hamilton", "12 July", "", "900");

        transactions.add(transaction1);
        transactions.add(transaction2);
        transactions.add(transaction3);
        transactions.add(transaction4);
        transactions.add(transaction5);
        transactions.add(transaction6);
        transactions.add(transaction7);

        TransactionAdapter transactionAdapter = new TransactionAdapter(this, transactions);
        recyclerViewTransactions.setAdapter(transactionAdapter);
        recyclerViewTransactions.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTransactions.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}