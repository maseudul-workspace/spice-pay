package com.webinfotech.spicepay.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.spicepay.R;
import com.webinfotech.spicepay.domain.models.testing.Transaction;
import com.webinfotech.spicepay.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    Context mContext;
    ArrayList<Transaction> transactions;

    public TransactionAdapter(Context mContext, ArrayList<Transaction> transactions) {
        this.mContext = mContext;
        this.transactions = transactions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_transactions, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUserName.setText(transactions.get(position).name);
        holder.txtViewDate.setText(transactions.get(position).date);
        holder.txtViewAmount.setText("-₹ " + transactions.get(position).amount);
        holder.txtViewReason.setText(transactions.get(position).reason);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewUser, transactions.get(position).image, 100);
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_user)
        ImageView imgViewUser;
        @BindView(R.id.txt_view_user_name)
        TextView txtViewUserName;
        @BindView(R.id.txt_view_date)
        TextView txtViewDate;
        @BindView(R.id.txt_view_reason)
        TextView txtViewReason;
        @BindView(R.id.txt_view_amount)
        TextView txtViewAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
