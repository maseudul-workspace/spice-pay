package com.webinfotech.spicepay.presentation.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.spicepay.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Splash3Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Splash3Fragment extends Fragment {

    public Splash3Fragment() {
        // Required empty public constructor
    }

    public static Splash3Fragment newInstance(String param1, String param2) {
        Splash3Fragment fragment = new Splash3Fragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_splash3, container, false);
    }
}