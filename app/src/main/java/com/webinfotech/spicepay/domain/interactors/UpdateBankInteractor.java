package com.webinfotech.spicepay.domain.interactors;

public interface UpdateBankInteractor {
    interface Callback {
        void onBankUpdateSuccess();
        void onBankUpdateFail(String errorMsg);
    }
}
