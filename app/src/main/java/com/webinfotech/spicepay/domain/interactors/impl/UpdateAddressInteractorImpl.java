package com.webinfotech.spicepay.domain.interactors.impl;

import com.webinfotech.spicepay.domain.executors.Executor;
import com.webinfotech.spicepay.domain.executors.MainThread;
import com.webinfotech.spicepay.domain.interactors.UpdateAddressInteractor;
import com.webinfotech.spicepay.domain.interactors.base.AbstractInteractor;
import com.webinfotech.spicepay.domain.models.CommonResponse;
import com.webinfotech.spicepay.domain.models.UserInfo;
import com.webinfotech.spicepay.respository.AppRepository;
import com.webinfotech.spicepay.respository.AppRepositoryImpl;

public class UpdateAddressInteractorImpl extends AbstractInteractor implements UpdateAddressInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int userId;
    String name;
    String dob;
    String address;

    public UpdateAddressInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int userId, String name, String dob, String address) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.userId = userId;
        this.name = name;
        this.dob = dob;
        this.address = address;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressUpdateFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddressUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateAddress(authorization, userId, name, dob, address);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
