package com.webinfotech.spicepay.domain.interactors;

public interface UpdateAddressInteractor {
    interface Callback {
        void onAddressUpdateSuccess();
        void onAddressUpdateFail(String errorMsg);
    }
}
