package com.webinfotech.spicepay.domain.models.testing;

public class Transaction {

    public String image;
    public String name;
    public String date;
    public String reason;
    public String amount;

    public Transaction(String image, String name, String date, String reason, String amount) {
        this.image = image;
        this.name = name;
        this.date = date;
        this.reason = reason;
        this.amount = amount;
    }
}
