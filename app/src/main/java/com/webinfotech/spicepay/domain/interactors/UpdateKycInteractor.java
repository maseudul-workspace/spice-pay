package com.webinfotech.spicepay.domain.interactors;

public interface UpdateKycInteractor {
    interface Callback {
        void onKycUpdateSuccess();
        void onKycUpdateFail(String errorMsg);
    }
}
