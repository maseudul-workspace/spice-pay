package com.webinfotech.spicepay.domain.interactors.impl;

import com.webinfotech.spicepay.domain.executors.Executor;
import com.webinfotech.spicepay.domain.executors.MainThread;
import com.webinfotech.spicepay.domain.interactors.CheckLoginInteractor;
import com.webinfotech.spicepay.domain.interactors.base.AbstractInteractor;
import com.webinfotech.spicepay.domain.models.UserInfo;
import com.webinfotech.spicepay.domain.models.UserInfoWrapper;
import com.webinfotech.spicepay.respository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo, boolean isProfileComplete){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo, isProfileComplete);
            }
        });
    }

    @Override
    public void run() {
        UserInfoWrapper userInfoWrapper = mRepository.checkLogin(email);
        if (userInfoWrapper == null) {
            notifyError("Something went wrong");
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message);
        } else {
            postMessage(userInfoWrapper.userInfo, userInfoWrapper.isComplete);
        }
    }
}
