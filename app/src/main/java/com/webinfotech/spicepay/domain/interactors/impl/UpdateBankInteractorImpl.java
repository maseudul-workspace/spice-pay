package com.webinfotech.spicepay.domain.interactors.impl;

import com.webinfotech.spicepay.domain.executors.Executor;
import com.webinfotech.spicepay.domain.executors.MainThread;
import com.webinfotech.spicepay.domain.interactors.UpdateBankInteractor;
import com.webinfotech.spicepay.domain.interactors.base.AbstractInteractor;
import com.webinfotech.spicepay.domain.models.CommonResponse;
import com.webinfotech.spicepay.respository.AppRepositoryImpl;

public class UpdateBankInteractorImpl extends AbstractInteractor implements UpdateBankInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String accountNo;
    String ifsc;
    String bankName;
    String bankHolderName;
    int accountType;

    public UpdateBankInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, String accountNo, String ifsc, String bankName, String bankHolderName, int accountType) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.accountNo = accountNo;
        this.ifsc = ifsc;
        this.bankName = bankName;
        this.bankHolderName = bankHolderName;
        this.accountType = accountType;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBankUpdateFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBankUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateBankAccount(apiToken, userId, accountNo, ifsc, bankName, bankHolderName, accountType);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
