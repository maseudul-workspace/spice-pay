package com.webinfotech.spicepay.domain.interactors.impl;

import com.webinfotech.spicepay.domain.executors.Executor;
import com.webinfotech.spicepay.domain.executors.MainThread;
import com.webinfotech.spicepay.domain.interactors.UpdateKycInteractor;
import com.webinfotech.spicepay.domain.interactors.base.AbstractInteractor;
import com.webinfotech.spicepay.domain.models.CommonResponse;
import com.webinfotech.spicepay.respository.AppRepository;
import com.webinfotech.spicepay.respository.AppRepositoryImpl;

public class UpdateKycInteractorImpl extends AbstractInteractor implements UpdateKycInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    String profilePhoto;
    String pan;
    String aadharFront;
    String aadharBack;

    public UpdateKycInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, String profilePhoto, String pan, String aadharFront, String aadharBack) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.profilePhoto = profilePhoto;
        this.pan = pan;
        this.aadharFront = aadharFront;
        this.aadharBack = aadharBack;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onKycUpdateFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onKycUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateKyc(apiToken, userId, profilePhoto, pan, aadharFront, aadharBack);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
