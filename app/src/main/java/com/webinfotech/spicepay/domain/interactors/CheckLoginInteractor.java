package com.webinfotech.spicepay.domain.interactors;

import com.webinfotech.spicepay.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo, boolean isProfileComplete);
        void onLoginFail(String errorMsg);
    }
}
