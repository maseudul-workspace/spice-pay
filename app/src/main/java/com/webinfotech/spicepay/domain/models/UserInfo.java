package com.webinfotech.spicepay.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-02-2019.
 */

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int userId;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("dob")
    @Expose
    public String dob;

    @SerializedName("ac_no")
    @Expose
    public String acNo;

    @SerializedName("ifsc")
    @Expose
    public String ifsc;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("bank_name")
    @Expose
    public String bankName;

    @SerializedName("bank_holder_name")
    @Expose
    public String bankHolderName;

    @SerializedName("photo")
    @Expose
    public String photo;

    @SerializedName("pan")
    @Expose
    public String pan;

    @SerializedName("aadhar_front")
    @Expose
    public String aadharFront;

    @SerializedName("aadhar_back")
    @Expose
    public String aadharBack;

    @SerializedName("status")
    @Expose
    public int status;

}
