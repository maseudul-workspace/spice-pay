package com.webinfotech.spicepay.domain.models.testing;

public class UpiList {

    public String upiImage;
    public String title;

    public UpiList(String upiImage, String title) {
        this.upiImage = upiImage;
        this.title = title;
    }
}
