package com.webinfotech.spicepay.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfoWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("is_complete")
    @Expose
    public boolean isComplete;

    @SerializedName("data")
    @Expose
    public UserInfo userInfo;

}
