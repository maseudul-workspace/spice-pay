package com.webinfotech.spicepay.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;

import androidx.fragment.app.FragmentManager;
import androidx.loader.content.CursorLoader;

public class Helper {

    public static void showLoginBottomSheet(FragmentManager fragmentManager) {

    }

    public static float calculateFileSize(String filepath) {
        //String filepathstr=filepath.toString();
        File file = new File(filepath);

        float fileSizeInKB = file.length() / 1024;
        // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
        float fileSizeInMB = fileSizeInKB / 1024;


        return fileSizeInMB;

    }

    public static String getRealPathFromURI(Uri contentUri, Context mContext) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(mContext, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    public static String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        try {
            File uploadDirectory = new File(Environment.getExternalStorageDirectory().getPath() + File.separator + "SalonEase" + File.separator + "Uploads");
            if (!uploadDirectory.exists()) {
                uploadDirectory.mkdirs();
            }

            File uploadablefile = new File(uploadDirectory, "temp.jpg");
            if (uploadablefile.exists()) {
                uploadablefile.delete();
            }

            uploadablefile.createNewFile();


            FileOutputStream fo = new FileOutputStream(uploadablefile);
            fo.write(bytes.toByteArray());
            fo.close();
            return uploadablefile.getAbsolutePath();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return "";
    }

}
